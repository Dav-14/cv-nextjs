import Link from "next/link";


function Posts({posts}){

    return (
        <div>
            <h3>Blog</h3>
            { posts&&Array.isArray(posts)&&posts.length>0 ?(
                <ul>
                    {
                        posts.map((post, i)=>(
                            <li key={i}>
                                <Link href={`/posts/${encodeURIComponent(post.uriName)}`}>
                                    <a>{post.title}</a>
                                </Link>
                                <p>{post.description}</p>
                            </li>
                        ))
                    }
                </ul>
                ) :
                (
                    <p>Aucun posts</p>
                ) 
            }
            
        </div>
    );
    
}

export async function getStaticProps() {
    // Get external data from the file system, API, DB, etc.
    const data = await fetch(process.env.NEXT_PUBLIC_APP_URL + '/api/hello');
    //console.log("DATA :", await data.json())
    const json = await data.json()
    // The value of the `props` key will be
    //  passed to the `Home` component

    return {
      props: {
          posts: json.posts,
      }
    }
}


export default Posts;