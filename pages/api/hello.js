// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  res.status(200).json({ name: 'John Doe', posts:[
    {
      uriName:"jesaispas",
      title:"My first Post",
      description:"Una biga description"
    },
    {
      uriName:"jesaispas2",
      title:"My second Post",
      description:"Una biga description"
    },
  ]})
}
